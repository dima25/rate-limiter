using System;

namespace dk.ratelimiter.Interfaces
{
    public interface IThrottleStrategy
    {
        long CurrentTokenCount { get; }

        bool ShouldThrottle(long n = 1);
        bool ShouldThrottle(long n, out TimeSpan waitTime);
        bool ShouldThrottle(out TimeSpan waitTime);
    }
}