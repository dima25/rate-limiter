using System;
using dk.ratelimiter.Interfaces;

namespace dk.ratelimiter
{
    public class TokenBucket : IThrottleStrategy
    {
        private long _tokenCount;
        private long _nextRefillTime;
        private readonly long _bucketTokenCapacity;
        private readonly long _ticksRefillInterval;
        private static readonly object _syncRoot = new object();

        public TokenBucket(long bucketTokenCapacity, long refillInterval, long refillIntervalInMilliSeconds)
        {
            _bucketTokenCapacity = bucketTokenCapacity;
            _ticksRefillInterval = TimeSpan.FromMilliseconds(refillInterval * refillIntervalInMilliSeconds).Ticks;
        }
        
        public long CurrentTokenCount
        {
            get
            {
                lock (_syncRoot)
                {
                    RefillTokensIfNeed();
                    return _tokenCount;
                }
            }
        }

        public bool ShouldThrottle(long n = 1)
        {
            return ShouldThrottle(n, out _);
        }

        public bool ShouldThrottle(out TimeSpan waitTime)
        {
            return ShouldThrottle(1, out waitTime);
        }
        
        public bool ShouldThrottle(long n, out TimeSpan waitTime)
        {
            lock (_syncRoot)
            {
                RefillTokensIfNeed();
                if (_tokenCount < n)
                {
                    var timeToIntervalEnd = _nextRefillTime - DateTime.UtcNow.Ticks;
                    if (timeToIntervalEnd < 0)
                    {
                        return ShouldThrottle(n, out waitTime);
                    }

                    waitTime = TimeSpan.FromTicks(timeToIntervalEnd);
                    return true;
                }
                _tokenCount -= n;

                waitTime = TimeSpan.Zero;
                return false;
            }
        }
        
        private void RefillTokensIfNeed()
        {
            var currentTime = DateTime.UtcNow.Ticks;
            if (currentTime < _nextRefillTime)
            {
                return;
            }

            _tokenCount = _bucketTokenCapacity;
            _nextRefillTime = currentTime + _ticksRefillInterval;
        }
    }
}