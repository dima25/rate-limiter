using dk.ratelimiter.Interfaces;

namespace dk.ratelimiter
{
    public class Throttler
    {
        private readonly IThrottleStrategy _strategy;

        public Throttler(IThrottleStrategy strategy)
        {
            _strategy = strategy;
        }
        
        public bool CanConsume()
        {
            return !_strategy.ShouldThrottle();
        }
    }
}