using System;
using System.Threading;
using NUnit.Framework;

namespace dk.ratelimiter.tests
{
    public class TokenBucketTests
    {
        private TokenBucket _bucket;
        public const long MAX_TOKENS = 10;
        public const long REFILL_INTERVAL = 10;
        public const long N_LESS_THAN_MAX = 2;
        public const long N_GREATER_THAN_MAX = 12;
        private const int CUMULATIVE = 2;
        
        [SetUp]
        public void SetUp()
        {
            _bucket = new TokenBucket(MAX_TOKENS, REFILL_INTERVAL, 1000);
        }
        
        [Test]
        public void ShouldThrottle_WhenCalledWithNTokensLessThanMax_ReturnsFalse()
        {
            TimeSpan waitTime;
            var shouldThrottle = _bucket.ShouldThrottle(N_LESS_THAN_MAX, out waitTime);

            Assert.That(shouldThrottle, Is.False);
            Assert.That(_bucket.CurrentTokenCount, Is.EqualTo(MAX_TOKENS - N_LESS_THAN_MAX));
        }
        
        [Test]
        public void ShouldThrottle_WhenCalledWithNTokensGreaterThanMax_ReturnsTrue()
        {
            TimeSpan waitTime;
            var shouldThrottle = _bucket.ShouldThrottle(N_GREATER_THAN_MAX, out waitTime);

            Assert.That(shouldThrottle, Is.True);
            Assert.That(_bucket.CurrentTokenCount, Is.EqualTo(MAX_TOKENS));
        }
        
        [Test]
        public void ShouldThrottle_WhenThread1NLessThanMaxAndThread2NLessThanMax()
        {
            var t1 = new Thread(p =>
            {
                var throttle = _bucket.ShouldThrottle(N_LESS_THAN_MAX);
                Assert.That(throttle, Is.False);
            });            
            
            var t2 = new Thread(p =>
            {
                var throttle = _bucket.ShouldThrottle(N_LESS_THAN_MAX);
                Assert.That(throttle, Is.False);
            });

            t1.Start();
            t2.Start();

            t1.Join();
            t2.Join();

            Assert.That(_bucket.CurrentTokenCount, Is.EqualTo(MAX_TOKENS - 2 * N_LESS_THAN_MAX));
        }
        
        [Test]
        public void ShouldThrottle_Thread1NGreaterThanMaxAndThread2NGreaterThanMax()
        {
            var shouldThrottle = _bucket.ShouldThrottle(N_GREATER_THAN_MAX);
            Assert.That(shouldThrottle, Is.True);

            var t1 = new Thread(p =>
            {
                var throttle = _bucket.ShouldThrottle(N_GREATER_THAN_MAX);
                Assert.That(throttle, Is.True);
            });            
            
            var t2 = new Thread(p =>
            {
                var throttle = _bucket.ShouldThrottle(N_GREATER_THAN_MAX);
                Assert.That(throttle, Is.True);
            });

            t1.Start();
            t2.Start();

            t1.Join();
            t2.Join();

            Assert.That(_bucket.CurrentTokenCount, Is.EqualTo(MAX_TOKENS));
        }
    }
}